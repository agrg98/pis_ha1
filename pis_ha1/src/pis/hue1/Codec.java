/**
 * 
 */
package pis.hue1;

/**
 * @author Aakas
 *
 */
public interface Codec {
	/**
	 * Kodiert den übergebenen Klartext
	 * @param klartext
	 * @return geheimtext
	 */
	public String kodiere(String klartext);
	/**
	 * Dekodiert den übergebenen Geheimtext
	 * @param geheimtext
	 * @return dokodierten Text
	 */
	public String dekodiere(String geheimtext);
	/**
	 * Gibt das Losungswort wieder
	 * @return losung
	 */
	public String gibLosung();
	/**
	 * Setzt Losung oder gibt Fehler bei ungeeignetem Schlüssel
	 * @param schluessel
	 * @throws IllegalArgumentException
	 */
	public void setzeLosung(String schluessel) throws IllegalArgumentException; // bei ungeeignetem Schlüssel!
}
