package pis.hue1;
/**
 * 
 * @author Aakas
 * Ein Programm um einen Text mit der Caesar Verschlüsselung zu verschlüsseln oder entschlüsseln
 */
public class Caesar implements Codec {
	
	/**
	 * Die Losung in der Caesar Verschlüsselung muss aus mindestens einem Zeichen bestehen
	 */
	private String losung;
	@Override
	public String kodiere(String klartext) {
		// TODO Auto-generated method stub
		int verschiebung = gibLosung().length();
		
		//StringBuilder um den Geheimtext auszugeben
		StringBuilder code = new StringBuilder();
		int i=0;
		while(i<klartext.length()) {
			//Falls es ein Kleinbuchstabe ist
			if(klartext.charAt(i)>='a'&& klartext.charAt(i)<='z') {
				code.append((char) ((((klartext.charAt(i)-97)+verschiebung)%26)+97));
			}
			//Falls es ein Großbuchstabe ist
			else if(klartext.charAt(i)>='A'&& klartext.charAt(i)<='Z') {
				code.append((char) ((((klartext.charAt(i)-65)+verschiebung)%26)+65));
			}
			//Falls es kein gültiger Buchstabe ist
			else {
				code.append(klartext.charAt(i));
			}
			
			i++;
		}
		return code.toString();
	}

	@Override
	public String dekodiere(String geheimtext) {
		// TODO Auto-generated method stub
		int verschiebung = 26-gibLosung().length();
		
		//StringBuilder um den Klartext auszugeben
		StringBuilder code = new StringBuilder();
		int i=0;
		while(i<geheimtext.length()) {
			//Falls es ein Kleinbuchstabe ist
			if(geheimtext.charAt(i)>='a'&& geheimtext.charAt(i)<='z') {
				code.append((char) ((((geheimtext.charAt(i)-97)+verschiebung)%26)+97));
			}
			//Falls es ein Großbuchstabe ist
			else if(geheimtext.charAt(i)>='A'&& geheimtext.charAt(i)<='Z') {
				code.append((char) ((((geheimtext.charAt(i)-65)+verschiebung)%26)+65));
			}
			//Falls es kein gültiger Buchstabe ist
			else {
				code.append(geheimtext.charAt(i));
			}
			
			i++;
		}
		return code.toString();
		
	}

	@Override
	public String gibLosung() {
		// TODO Auto-generated method stub
		return this.losung;
	}

	@Override
	public void setzeLosung(String schluessel) throws IllegalArgumentException {
		// TODO Auto-generated method stub
		if( schluessel.equals("")) {
			throw  new IllegalArgumentException();
		}
		this.losung = schluessel;
	}

}
