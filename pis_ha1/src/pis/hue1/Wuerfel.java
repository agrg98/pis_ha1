package pis.hue1;

import java.util.Arrays;
import java.util.Comparator;
/**
 * 
 * @author Aakas
 * Ein Programm um einen Text mit der Wuerfel Verschlüsselung zu verschlüsseln oder entschlüsseln
 */
public class Wuerfel implements Codec {
	/**
	 * Die losungen müssen aus mindestens 2 Zeichen bestehen
	 */
	private String losung;
	
	
	public Wuerfel(String string) {
		// TODO Auto-generated constructor stub
		setzeLosung(string);
	}
	public Wuerfel() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * Erzeugt die Zahlenlosung; interpretiert den Schlüssel als Reihenfolge der Ausgabe
	 * @param losung
	 * @return
	 */
	public int[] gibZahlenlosung() {
		//Zahlenlosung Array initialisieren mit der Länge der losung
		int[] zahlenlosung = new int[gibLosung().length()];
		//Initialisiere Zahlenlosung mit -1, um mögliche Komplikationen später zu verhindern
		for(int i=0; i<zahlenlosung.length;i++) {
			zahlenlosung[i] = -1;
		}
		//Groß und Kleinschreibung bei Losung irrelevant, also mögliche Unterschiede angleichen und als Array initialisieren
		char[] sortlos = gibLosung().toLowerCase().toCharArray();
		//losung als Char array initialisieren um diesen mit dem sortierten Array zu vergleichen
		char[] charlos = gibLosung().toLowerCase().toCharArray();
		//CharArray sortieren
		Arrays.sort(sortlos);	
		//Sortiertes Array durchgehen
		for(int i=0; i<sortlos.length;i++) {
			//Stelle in losung finden in denen die chars des sortierten Arrays vorkommen
			for(int j=0; j<charlos.length;j++) {
				//falls der Buchstabe in losung gefunden wurde und an dieser Stelle noch nicht die Reihenfolge in zahlenlosung geschrieben wurde
				if(sortlos[i] == charlos[j] && zahlenlosung[j] == -1 ) {
					//schreibe die reihenfolge rein falls getan beende
					zahlenlosung[j] = i;
					break;
				}
			}
		}		
		
		return zahlenlosung;
	}
	@Override
	public String kodiere(String klartext) {
		// TODO Auto-generated method stub
		
		StringBuilder code = new StringBuilder();
		
		//Iteriere so oft durch wie viele Spalten es gibt
		for(int i=0;i<gibZahlenlosung().length;i++) {
			//Suche die Spalten aufsteigend
			for(int j=0; j<gibZahlenlosung().length;j++) {
				//Falls die aktuelle Spalte gefunden wurde an der pos j
				if(gibZahlenlosung()[j] ==i) {
					//dann durchlaufe den Klartext und gebe alle Elemente des Strings aus die in dieser spalte wären
					while(j<klartext.length()) {
						code.append(klartext.charAt(j));	
						//Erhöhe den Interator um eine Reihenlänge um den Effekt eines Spaltenwechsels zu erzielen
						j=j+gibLosung().length();
					}
					
				}
			}
		}
		return code.toString();
	}

	@Override
	public String dekodiere(String geheimtext) {
		// TODO Auto-generated method stub
		StringBuilder code = new StringBuilder();
		code.append(geheimtext);
		// Iterator für den Geheimtext
		int iter = 0;
		// Iteriere so oft durch wie viele Spalten es gibt
		for (int i = 0; i < gibZahlenlosung().length; i++) {
			// Suche die Spalten aufsteigend
			for (int j = 0; j < gibZahlenlosung().length; j++) {
				// Falls die aktuelle Spalte gefunden wurde an der pos j
				if (gibZahlenlosung()[j] == i) {
					// Dann füge an der Pos dieser Spalte alle dazugehörigen Elemente vom Geheimtext hinzu bis die Spalte endet
					while (j < geheimtext.length()) {
						//Füge der Reihe nach die Elemente des Geheimtextes zur jeweiligen Spalte
						code.setCharAt(j, geheimtext.charAt(iter));
						// Erhöhe den Interator um eine Reihenlänge um den Effekt eines Spaltenwechsels
						// zu erzielen und gehe zum nächsten Element im Geheimtext um diesen in den Klartext zu übergeben
						j = j + gibLosung().length();
						iter++;
						
					}

				}
			}
		}
		System.out.println(code.toString());
		return code.toString();

	}

	@Override
	public String gibLosung() {
		// TODO Auto-generated method stub
		return this.losung;
	}

	@Override
	public void setzeLosung(String schluessel) throws IllegalArgumentException {
		// TODO Auto-generated method stub
		if(schluessel.length() <=1) {
			throw new IllegalArgumentException();
		}
		this.losung = schluessel;
	}

}
