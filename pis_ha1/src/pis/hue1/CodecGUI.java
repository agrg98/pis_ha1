package pis.hue1;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
/**
 * 
 * @author Aakas
 * Diese Klasse ist für das Userinterface von den Verschlüsselungsmethoden zuständig
 */
public class CodecGUI extends Application{
	
	Codec wuerfela;
	Codec wuerfelb;
	Codec caesar;
	
	/**
	 * Der Konstruktor der Klasse initialisiert 2 Wuerfel Objekte (1 für die jeweilige Lösung) und 1 Caesar Objekt
	 */
	public CodecGUI() {
		wuerfela = new Wuerfel();
		wuerfelb = new Wuerfel();
		caesar = new Caesar();
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		primaryStage.setTitle("Würfel- und Cäsar-Verschlüsselung");
		
		//Hier wird das Grid initialisiert und angepasst		
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20,20,20,20));
		grid.setGridLinesVisible(false);
		
		//Alle Objekte im Grid
		Label klarlbl = new Label("Schreibe hier den Klartext hinein: ");
		grid.add(klarlbl, 0, 0,1,1);
		
		TextArea klartxt = new TextArea();
		klartxt.setMinSize(300, 200);
		grid.add(klartxt, 0, 1,1,1);
		
		Label geheimlbl = new Label("Schreibe hier den Geheimtext hinein: ");
		grid.add(geheimlbl, 1, 0,1,1);
		
		TextArea geheimtxt = new TextArea();
		geheimtxt.setMinSize(300, 200);
		grid.add(geheimtxt, 1, 1,1,1);
		
		Label losunga = new Label("Schreibe hier die erste Losung hinein: ");
		grid.add(losunga, 0, 2,1,1);
		
		TextField atxt = new TextField();
		atxt.setMinSize(300, 70);
		grid.add(atxt, 0, 3,1,1);
		
		Label losungb = new Label("Schreibe hier die zweite Losung hinein: ");
		grid.add(losungb, 1, 2,1,1);
		
		TextField btxt = new TextField();
		btxt.setMinSize(300, 70);
		grid.add(btxt, 1, 3,1,1);
		
		Button codebtn = new Button("Verschluesseln");
		HBox cbtn = new HBox();
		cbtn.setAlignment(Pos.CENTER);
		cbtn.getChildren().add(codebtn);
		grid.add(cbtn, 0, 4,1,1);
		
		Button encodebtn = new Button("Entschluesseln");
		HBox ebtn = new HBox();
		ebtn.setAlignment(Pos.CENTER);
		ebtn.getChildren().add(encodebtn);
		grid.add(ebtn, 1, 4,1,1);
		
		Label ergeblbl = new Label("Hier ist der Ergebnistext: ");
		grid.add(ergeblbl, 0, 5,2,1);
		
		TextArea ergebtxt = new TextArea();
		ergebtxt.setMinSize(300, 200);
		grid.add(ergebtxt, 0, 6,2,1);
		
		Label cmdlbl = new Label("Willkommen \n"
				+ "Bitte wählen Sie hier unten die gewünschte Verschlüsselungsmethode aus");
		cmdlbl.setWrapText(true);
		cmdlbl.setMinWidth(200);
		grid.add(cmdlbl, 2, 0,2,3);
		
		Button stopbtn = new Button("Stop");
		HBox sbtn = new HBox();
		sbtn.setAlignment(Pos.CENTER);
		sbtn.getChildren().add(stopbtn);
		grid.add(sbtn, 2, 6,2,1);
		
		ToggleGroup group = new ToggleGroup();
		
		RadioButton caesarbtn = new RadioButton("Caesar");
		caesarbtn.setWrapText(true);
		caesarbtn.setMinWidth(100);
		grid.add(caesarbtn, 2,4,1,1);
		
		RadioButton wuerfelbtn = new RadioButton("Wuerfel");
		wuerfelbtn.setWrapText(true);
		wuerfelbtn.setMinWidth(100);
		grid.add(wuerfelbtn, 3,4,1,1);
		
		caesarbtn.setToggleGroup(group);
		wuerfelbtn.setToggleGroup(group);
		
		//Eine Losung wird deaktiviert, da caesar nur eine Losung braucht
		caesarbtn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				// TODO Auto-generated method stub
				
					btxt.setDisable(true);
			}
			
		});
		//Das 2. Losungsfeld wird freigegeben, wenn man die Wuerfel-Verschlüsselung nutzen möchte
		wuerfelbtn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				// TODO Auto-generated method stub
				
					btxt.setDisable(false);
			}
			
		});
		//Nimmt den Klartext und verschlüsselt ihn mit der jeweiligen Verschlüsselungsart
		codebtn.setOnAction(new EventHandler<ActionEvent>(){

			@Override
			public void handle(ActionEvent e) {
				// TODO Auto-generated method stub
				if(caesarbtn.isSelected()==false && wuerfelbtn.isSelected()==false) {
					cmdlbl.setText("Bitte wählen Sie vorher eine Art der Verschlüsselung aus");
				}else {
					cmdlbl.setText("Nun können Sie belieblig verschlüsseln oder entschlüssen");
					if(caesarbtn.isSelected()) {
						
						try {
							caesar.setzeLosung(atxt.getText());
							String erg = caesar.kodiere(klartxt.getText());
							ergebtxt.setText(erg);
						} catch (IllegalArgumentException e1) {
							// TODO Auto-generated catch block
							cmdlbl.setText("Fehlber beim Setzen der Losung");
						}
					}
					if(wuerfelbtn.isSelected()) {
						try {
							wuerfela.setzeLosung(atxt.getText());
							String erg1 = wuerfela.kodiere(klartxt.getText());
							wuerfelb.setzeLosung(btxt.getText());
							String erg2 = wuerfelb.kodiere(erg1);
							ergebtxt.setText(erg2);
						} catch (IllegalArgumentException e1) {
							// TODO Auto-generated catch block
							cmdlbl.setText("Fehlber beim Setzen der Losung");
						}
					}
				}
			}
			
		});
		//Nimmt den Geheimtext und entschlüsselt ihn mit der jeweiligen Verschlüsselungsart
		 encodebtn.setOnAction(new EventHandler<ActionEvent>(){

			@Override
			public void handle(ActionEvent e) {
				// TODO Auto-generated method stub
				if(caesarbtn.isSelected()==false && wuerfelbtn.isSelected()==false) {
					cmdlbl.setText("Bitte wählen Sie vorher eine Art der Verschlüsselung aus");
				}else {
					cmdlbl.setText("Nun können Sie belieblig verschlüsseln oder entschlüssen");
					if(caesarbtn.isSelected()) {
						
						try {
							caesar.setzeLosung(atxt.getText());
							String erg = caesar.dekodiere(geheimtxt.getText());
							ergebtxt.setText(erg);
						} catch (IllegalArgumentException e1) {
							// TODO Auto-generated catch block
							cmdlbl.setText("Fehlber beim Setzen der Losung");
						}
					}
					if(wuerfelbtn.isSelected()) {
						try {
							wuerfela.setzeLosung(btxt.getText());
							String erg1 = wuerfela.dekodiere(geheimtxt.getText());
							wuerfelb.setzeLosung(atxt.getText());
							String erg2 = wuerfelb.dekodiere(erg1);
							ergebtxt.setText(erg2);
							
						} catch (IllegalArgumentException e1) {
							// TODO Auto-generated catch block
							cmdlbl.setText("Fehlber beim Setzen der Losung");
						}
							
					}
				}
			
			}
			
		});
		//Beende das Programm mit einem Klick auf dem Stop Button
		 stopbtn.setOnAction(new EventHandler<ActionEvent>(){

			@Override
			public void handle(ActionEvent arg0) {
				// TODO Auto-generated method stub
				primaryStage.close();
			}
			 
			 
			 
		 });
		 //Erstelle die GUI 
		Scene scene = new Scene(grid, 1400, 800);
		primaryStage.setScene(scene);
		primaryStage.show();
		
	}
	
	public static void main(String[] args) {
		Application.launch(args);
	}
}
